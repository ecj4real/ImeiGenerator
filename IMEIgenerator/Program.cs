﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMEIgenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Random ran = new Random();
            int firstTwoDigits = ran.Next(10, 99);
            int firstSixDigits = ran.Next(100000, 999999);
            int secondSixDigits = ran.Next(100000, 999999);
            int lastDigit = ran.Next(0, 9);

            string output = "" + firstTwoDigits + firstSixDigits + secondSixDigits + lastDigit;
            Console.WriteLine("IMEI Number: "+output);
            Console.ReadKey();
        }
    }
}
